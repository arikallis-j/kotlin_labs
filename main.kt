import java.util.*
import kotlin.math.round

fun main() {
    val rnd = Random(30)
    val freq = 10
    val p = 50 //%
    val n = 30//54
    val m = 30//119
    val steps = 100
    val Survival = listOf(2,3)
    val Burning = listOf(3)
    
    var A = Array(n) {IntArray(m)}
    for (i in 0 until n) {
        for (j in 0 until m) {
            val x = round(rnd.nextDouble() + (p - 50) / 100).toInt()
            A[i][j] = x
        }
    }
    
    fun indi(x: Int): Int {
        var y = x
        if (y >= n) {
            y -= n
        }
        if (y<0) {
            y += n
        }
        return y
    }
    
    fun indj(x: Int): Int {
        var y = x
        if (y >= m) {
            y -= m
        }
        if (y<0) {
            y += m
        }
        return y
    }
    
    fun new(A: Array<IntArray>): Array<IntArray> {
        val B = Array(n) {IntArray(m)}
        for (i in 0 until n) {
            for (j in 0 until m) {
                val a = A[indi(i - 1)][indj(j - 1)]
                val b = A[indi(i - 1)][indj(j)]
                val c = A[indi(i - 1)][indj(j + 1)]
                val d = A[indi(i)][indj(j - 1)]
                val e = A[indi(i)][indj(j)]
                val f = A[indi(i)][indj(j + 1)]
                val g = A[indi(i + 1)][indj(j - 1)]
                val h = A[indi(i + 1)][indj(j)]
                val l = A[indi(i + 1)][indj(j + 1)]
                val sum = a + b + c + d + f + g + h + l
                B[i][j] = when (e) {
                    0 -> if (Burning.contains(sum)) 1 else 0
                    else -> if (Survival.contains(sum)) 1 else 0
                }
            }
        }
        return B
    }

    fun printM(A: Array<IntArray>) {
        val line = StringBuilder()
        line.append(" _".repeat(m))
        line.append("\n")
        for (i in 0 until n) {
            
            line.append("|")
            for (j in 0 until m) {
                
                if (A[i][j] == 0) {
                    line.append("_|")
                } else {
                    line.append("@|")
                }
                if (j == m - 1) {
                    line.append("\n")
                }
            }
           
        } 
        print(line)
    }
    
    for (i in 0 until steps){
        printM(A)
        A = new(A)
        print("\n")
        Thread.sleep((1000 / freq).toLong())
    }
}